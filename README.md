# Cisco Config Utils

> Note: This project of mine has just started. Therefore it is in a *very* early stage and might be discontinued at any time.

This package provides a library to handle Cisco configuration files in a better programmatic way.

## Contained Packages
* **Cisco Config Parser**
* **Cisco Config Templater**

As the names suggest, *Cisco Config Parser* can be used to search, audit and parse existing configuration files, whereas *Cisco Config Templater* can be used to generate these configs.
